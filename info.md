Hyperledger Fabric is the most widely used private blockchain. Used primarily in enterprise settings to make transactions between multiple businesses more seamless and efficient.

To accommodate each cases custom needs,Fabric has a modular design which means that businesses can plug in different functionalities to suit their particular needs.

Let's take a look at what these modules are and how they interact with each other.
Like all blockchains, fabric records a history of transactions in a chronological ledger.

In Bitcoin, for example, the ledger holds the record of bitcoins as they transfer from one party to another.

In fabric, the definition of what gets transferred is a bit looser and is defined as an asset.

An asset can be anything with monetary value. From whole foods to antique cars to currency futures.
Hyperledger Fabric allows the businesses to set their asset types and values themselves.
Assets are represented as a collection of key-value pairs, with state changes recorded as transactions on the ledger.

Hyperledger Fabric provides the ability to modify assets using chaincode. Chaincode is software defining an asset or assets, and the transaction instructions for modifying those assets. 

In other words, it's the business logic. Smart contracts that are deployed to the fabric Ledger execute chaincode.
Instead of each business having their own business logic, which changes its own database, the businesses share the business logic. And all sign off on changes to the database. 

Members of each permission network within Hyperledger Fabric can interact with the ledger using chaincode.
Either by deploying new contracts that add new business logic or invoking transactions that were already codified in earlier contracts.

To enable these permission networks, Hyperledger Fabric provides a membership identity service that manages user IDs and authenticates all participants on the network.

Access control lists can be used to provide additional layers of permission through authorization of specific network operations. For example, a specific user ID could be permitted to invoke a chaincode application
but be blocked from deploying new chaincode.

This permission network also assigns network roles by node types.

There are two node types within the Hyperledger Fabric network.
Peer nodes and ordering nodes.
Peer nodes are responsible for executing and verifying transactions.
While ordering nodes are responsible for ordering transactions and propagating the correct history of events to the network. This is done to increase efficiency and scalability by allowing peer nodes to batch and process multiple transactions simultaneously.

The network consensus protocol, which the businesses and the network can customize, is then implemented by the ordering nodes to create a single true record of transaction.

Fabrics Ledger is comprised of two components. A Blockchain Log to stores the immutable sequenced record
of transactions in blocks and a State Database to maintain the blockchain's current state.

In the Bitcoin blockchain, there is no database. And the current state of the chain is always calculated by going through all the transactions in the Ledger.

For speed and efficiency's sake, Hyperledger Fabric stores the current state as well and allows the members of the network to query it and sequel-like transactions. The purpose of the log here is to trap an assets providence or place of origin as it is exchanged amongst multiple parties.

To track an assets' provenance means to track where and when it was created as well as every time it was exchanged.
Tracking an assets' providence is extremely important in the world of business because it ensures that the business selling an item possesses a chain of titles verifying their ownership of it.

In typical databases, where only the current state of an asset is kept and not a log of all transactions, tracking an assets providence becomes very difficult. Add to this the fact that transacting businesses each keep an incomplete record of the assets transaction and it becomes nearly impossible.

This is the reason many businesses stuck to this broken model of data collection for as long as they did
was for the sake of privacy. Having a distributed Ledger meant that every party in a business network would have access to all the transactions even if they weren't involved in the transactions themselves.

This was a deal breaker for many businesses who were perhaps in the same business network as their competitors and didn't want to reveal their data or transactions with other parties to their competitors.

Hyperledger Fabric solves this problem by using Private Channels which are restricted messaging pods
that can be used to provide transaction privacy and confidentiality for specific subsets of network members.

All data including transactions, member, and channel information on a channel, are invisible and inaccessible to any network members not explicitly granted access to that channel.

This allows competing business interests and any groups that require private confidential transactions to coexist
on the same permissions network.

To summarize, Hyperledger Fabric has a modular design that enables five core functionalities.
- Members of a business can define asset types and consensus protocol for ordering transactions.
- They can also set permissions on who can join the network and what type of access each membership can grant.
- To increase efficiency, nodes are divided into two types.
- Peer nodes and ordering nodes.
- With peer nodes batching, verifying and processing transactions and with ordering nodes logging and ordering those transactions in chronological order.

Fabric's ledger itself consists of a database of the networks current state which can be queried and the log of transactions stored as a blockchain for tracking each assets' providence. Assets on the network are added, updated,
and transferred using chaincode. And the form of smart contracts deployed and invoked on the network.
