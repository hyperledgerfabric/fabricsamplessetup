Open your ubuntu16.0

```
sudo apt-get install curl

sudo apt-get update

sudo apt-get install golang-go

export GOPATH=$HOME/go

export PATH=$PATH:$GOPATH/bin

sudo apt-get install nodejs

sudo apt-get install npm

sudo apt-get install python

sudo apt-get install docker

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get update

sudo apt-cache policy docker-ce

sudo apt-get install -y docker-ce

sudo apt-get install docker-compose

sudo apt-get upgrade
```

Downloading examples of fabric
```
sudo curl -sSL https://goo.gl/6wtTN5 | sudo bash -s 1.1.0

sudo chmod 777 -R fabric-samples
```

Change into the first-network directory and run the generate script that will create the certificates and keys for the entities that are going to exist on our blockchain. This will also create the genesis block (the first block on the blockchain), among other things.
```
cd fabric-samples/first-network

sudo service docker start [not needed]

sudo ./byfn.sh generate
```
Now let’s start the fabric
```
sudo ./byfn.sh up
```
You successfully created your first Fabric network! Congratulations.
To shutdown the fabric
```
sudo ./byfn.sh down
```

____________________________________________________________________________________________________________
First day of hyperledger

